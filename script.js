const arr = ['Dnipro', 'Kherson', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

function filterArr(arr) {
        const newArr = arr.map((el) => {
        el = `<li>${el}</li>`;
        return el;
    });
        const ul = document.createElement('ul');
        ul.innerHTML = newArr.join('');
        document.querySelector('.sections').appendChild(ul);
}

filterArr(arr);

/*
DOM - это некий интерфейс, который позволяет JavaScript получить содежимое html докуметка в виде узлов, их детей и родителей
и влиять на их поведение, изменяя или добавляя новые узлы, свойства или атрибуты.
 */
